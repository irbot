#!/bin/sh

echo ------------------------------------
echo Running Cleanup Script
echo ------------------------------------

find . -depth -type d -name .svn -exec rm -fr {} \;
find . -type d -exec chmod 775 {} \;
find . -type f -name "*.*~" -exec rm -fr {} \;
find . -type f -name "*.php" -exec chmod -x {} \;
find . -type f -name "main.php" -exec chmod +x {} \;

rm -fr release-cleanup.sh

echo ------------------------------------
echo Done!
echo ------------------------------------
