<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


class jet implements plugin {
	private $IRCConn,$formater;
	private $message;
	private $commands;

	public function __construct($main) {
		$this->IRCConn = bot::GetInstance();
		$this->formater = text_format::GetInstance();
		$this->commands = array(
			'string2dice' => array( // !jet string2dice
				'accepted_args' => 1,
				'requier_args' => 1,
				'help' => 'some help for this method1',
				'type' => 'private',
			),
			'caract' => array (
				'accepted_args' => 3,
				'requier_args' => 1,
				'help' => "Jet bas� sur une caract�ristique.\n".
						  "Argument premier : Valeur de la caract�ristique\n".
						  "Argument second : Base du jet, par d�faut une base 20 (peut �tre mis a 100 pour calculer le poid transportable par exemple)\n".
						  "Argument troisi�me : ".$this->formater->bold('[1|0]')." Sp�cifie si le jet doit retourner quelque chose d'al�atoire, sinon",
				'type' => 'private'
			)
		);
	}

	public function commands_list() {
		return $this->commands;
	}

	public function current_message ($message) {
		/* gived by irc::parse_get
		$message = array (
		 	'type' => PRIVMSG|NOTICE,
			'from' => Nick (reply to),
			'to' => BotName / ChannelName,
			'message' => The message
		);
		*/
		$this->message = $message;
	}

	public function string2dice ($str) {
		$str = strtolower($str);

		$numbers = array(0,1,2,3,4,5,6,7,8,9);
		$modifiers = array('+','-','*','/');

		if ($str == '1d1')
			return 1;

		$copy = $str;
		$i = 0;
		$len_copy = strlen($copy);

		$number_of_dice = '';
		$value_of_dice = '';
		$modifier = '';
		$modifier_nature = '';
		$step = 'first';
		while ($len_copy > $i) {
			/**
			 * Si le caract�re courant n'est pas un nombre, alors quelques v�rifications.
			 * Nous v�rifions la partie du d� que nous cr�ons : premi�re (avant le 'd'), deuxi�me (apr�s le 'd')
			 * et si un modifieur existe ('+','-','*','/').
			 */
			if ($copy[$i] == 'd' || in_array($copy[$i],$modifiers)) {
				// Nombre de d�s a lancer manquant
				if ($i == 0) {
					trigger_error('Wrong dice format : "'.$copy.'". Must be [Number]d[Value][Modifier][Number]',E_USER_WARNING);
					return false;
				}

				if ($copy[$i] == 'd') {
					$step = 'second';
				} elseif (in_array($copy[$i],$modifiers)) {
					// Valeur du d� manquant
					if (empty($value_of_dice)) {
						trigger_error('Wrong dice format : "'.$copy.'". Must be [Number]d[Value][Modifier][Number]',E_USER_WARNING);
						return false;
					}
					$step = 'modifier';
					$modifier_nature = $copy[$i];
				} else {
					trigger_error('Unknown caracter \'' . $copy[$i] . '\'.',E_USER_WARNING);
				}
			} else {
				if ($step == 'first') {
					$number_of_dice.= $copy[$i];
				}

				if ($step == 'second') {
					$value_of_dice.= $copy[$i];
				}

				if ($step == 'modifier') {
					$modifier.= $copy[$i];
				}
			}

			$i++;
		}

		$de = self::dice($number_of_dice,$value_of_dice);
		if ($modifier_nature == '') {
			$r = $de;
		} else {
			$r = eval('return abs(ceil($de'.$modifier_nature.'$modifier));');
		}

		if (bot::$myBotName == $this->message['to']) {
			$replyto = $this->message['from'];
		} else {
			$replyto = $this->message['to'];
		}

		$this->IRCConn->privmsg($replyto,$r);
	}

	/**
	 * Jet bas� sur une caract�ristique.
	 *
	 * @param int Valeur de la caract�ristique
	 * @param int Base du jet, par d�faut une base 20 (peut �tre mis a 100 pour calculer le poid transportable par exemple)
	 * @param boolean Sp�cifie si le jet doit retourner quelque chose d'al�atoire, sinon
	 */
	public function caract($caract,$base=20,$rand=true) {
		$caract = intval($caract);
		$base = intval($base);

		$de = ($rand) ? self::dice(1,20) : 0 ;
		$result = floor(($de+$caract) * $base / 20);
		// (n / 20) * base => la base par d�faut est 20


		if ($rand)
			$result = $result/2;

		$this->IRCConn->privmsg($this->message['from'],$result);
		return $result;
	}

	/**
	 * Lance un d�
	 * @param int $number nombre de d� lanc�.
	 * @param int $value valeur maximale du d�.
	 */
	private function dice($number,$value) {
		$number = intval($number);
		$value = intval($value);

		// Si la valeur du d� est a 0, on retourne tout de suite 0.
		if ($value === 0)
			return 0;

		$toreturn=0;
		while ($number>0) {
			$toreturn+= rand(1,$value);
			$number-=1;
		}
		return $toreturn;
	}

}

?>