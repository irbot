<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
CREATE TABLE IF NOT EXISTS `fortunes` (
  `id` int(11) NOT NULL auto_increment,
  `pseudo` varchar(25) NOT NULL,
  `ip` varchar(15) default NULL,
  `email` varchar(25) NOT NULL,
  `site` varchar(70) NOT NULL,
  `fortunes` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
*/

class fortune {
	private $IRCConn,$formater;
	private $message;
	private $commands;

	public function __construct() {
		$this->IRCConn = bot::GetInstance();
		$this->formater = text_format::GetInstance();
		$this->commands = array(
			'random' => array(
				'accepted_args' => 0,
				'requier_args'  => 0,
				'help' => 'Print out one random fortune',
				'type' => 'mixed',
			),
			'last' => array(
				'accepted_args' => 1,
				'requier_args'  => 0,
				'help' => 'Print out the last fortune added. If an argument is passed, print the \'n\' last fortune added (max 10).',
				'type' => 'mixed',
			),
			'number' => array(
				'accepted_args' => 1,
				'requier_args'  => 1,
				'help' => 'Print out the \'n\' fortune, where \'n\' is the id.',
				'type' => 'mixed',
			)
		);
	}

	public function commands_list() {
		return $this->commands;
	}

	public function current_message ($message) {
		/* gived by irc::parse_get
		$message = array (
		 	'type' => PRIVMSG|NOTICE,
			'from' => Nick (reply to),
			'to' => BotName / ChannelName,
			'message' => The message
		);
		*/
		$this->message = $message;
	}

	public function random() {}
	public function last($n=1) {}
	public function number($n) {}
}

?>