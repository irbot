<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category   IrBot
 * @package    IrBot_Plugins
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * @see Plugins_Abstract
 */
require_once(BASE_DIR.'Plugins/Abstract.php');

class sample extends Plugins_Abstract {
	protected $_ircmain;

	public function init() {
		$this->_ircmain = bot::GetInstance();
	}

	public function commands_list() {
		$commands = array(
			'method1' => array( // !sample method1
				'requier_args' => 0,
				'accepted_args' => 0,
				'help' => 'some help for this method1',
				'type' => 'public',
			),
			'method2' => array( // !sample method2 arg
				'requier_args' => 1,
				'accepted_args' => 1,
				// the \n chr in a double quote string create a new line (2 PRIVMSG)
				'help' => "some help for this method2\n".$this->formater->bold('one arg').' requier',
				'type' => 'public',
			),
			'method3' => array( // /msg BotName sample method3 arg
				'requier_args' => 0,
				'accepted_args' => 1,
				'help' => 'some help for this method3',
				'type' => 'mixed',
			),
		);
		return $commands;
	}

	public function current_message ($message) {
		/* gived by irc::parse_get
		$message = array (
		 	'type' => PRIVMSG|NOTICE,
			'from' => Nick (reply to),
			'to' => BotName / ChannelName,
			'message' => The message
		);
		*/
		$this->message = $message;
	}

	function method1 ($config = false) {
		$this->IRCConn->privmsg(bot::$channel,'This is the method1');
	}

	function method2 ($query) {
		$this->IRCConn->privmsg(bot::$channel,'This is the method2 : '.$query);
	}

	function method3 ($query='') {

		if ($query=='') {
			$query = 'no args given. All works !';
		}

		if (bot::$myBotName == $this->message['to']) {
			$replyto = $this->message['from'];
		} else {
			$replyto = $this->message['to'];
		}

		$this->IRCConn->privmsg($replyto,'This is the method3 : '.$query);
	}
	
	/**
	 * Return plugin name
	 *
	 * @return string
	 */
	public function toString() {
		return 'sample';
	}
}
?>