<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category   IrBot
 * @package    IrBot_Plugins
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * @see Plugins_Command_Abstract
 */
require_once(BASE_DIR.'Plugins/Command/Abstract.php');

class Plugin_Sample_Method1 extends Plugins_Command_Abstract {
	
	protected $_ircmain;
	
	public function __construct(Plugins_Abstract $plugin,IRCMain $ircmain) {
		parent::__construct($plugin);
		$this->_ircmain = $ircmain;
		$this->_helpMessage = 'some help for this method1';
		
		$options = array(
			self::REQUIERED_ARGS => 0,
			self::ACCEPTED_ARGS => 0,
			self::COMMAND_VISIBILITY => self::TYPE_PUBLIC
		);
		
		self::setOptions($options);
		
	}
	
	public function doAction() {
		if ($this->_event->isForChannel()) {
			$this->ircmain->privmsg($this->_event->getDataFor(),'This is the method1');
		} else {
			$this->ircmain->privmsg($this->_event->getDataFor(),'Sorry, but this command is for public channel only.');
		}
	}
	
	public function getType() {
		return $this->_defaults[self::COMMAND_VISIBILITY];
	}
	
	public function __toString() {
		return "method1";
	}
}
