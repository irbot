#!/bin/sh

version=$1

if [ "$version" = "" ]; then
	echo "No version given"
	exit 1
fi

svn export http://svn2.assembla.com/svn/irbot/trunk/ irbot-${version}
cd irbot-${version}
sh release-cleanup.sh
svn import http://svn2.assembla.com/svn/irbot/tags/version-${version} -m "-- Release ${version} --" --no-auto-props
cd ..

tar zcf irbot-${version}.tar.gz irbot-${version}
mv irbot-${version} irbot
zip -r irbot-${version}.zip irbot
md5sum irbot-${version}.*
rm -rf irbot
