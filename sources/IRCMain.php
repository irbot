<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category   IrBot
 * @package    IrBot_IRCMain
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

require_once 'sources/IRCMain/Adapter.php';

class IRCMain extends IRCMain_Adapter {

	public function __construct(array $options) {
		self::setConfig($options);
		$this->tick = tick::GetInstance($this);
	}
	
	private function _authentification(Event $event) {
		// TODO : create a better login process
		
		if ($event->getDataFor() == self::getConfig('nick')) {
			if (preg_match('`^connect ([^ ]+) ([^ ]+)`',trim($event->getDataMessage()),$m)) {
				if ($m[1] == 'admin' && $m[2] == 'mypass') {
					$this->auth = true;
					self::notice($event->getDataSendBy(),'Vous êtes bien authentifié comme administrateur.');
				} else {
					self::notice($event->getDataSendBy(),'Erreur dans votre login et / ou mot de passe.');
					echo debug() ? 'l: '.$m[1].' p: '.$m[2]."\n":'';
				}
				return true;
			}
			return false;
		}
	}
	
	public function launch() {
		parent::launch();
		
/*		$this->_pluginsInstance->add_command('core','shownick',0,'Show the current nick used (debug)','mixed');
		$this->_pluginsInstance->add_command('core','nick',1,'Change the current nick','mixed');
		$this->_pluginsInstance->add_command('core','quit',0,'Disconnect and stop the process','mixed');
		$this->_pluginsInstance->add_command('core','restart',0,'Disconnect and restart the process','mixed');
		$this->_pluginsInstance->add_command('core','help',0,'Hmm, je dois recoder cette fonction','mixed');
		$this->_pluginsInstance->Init();*/
		
		try {
			$this->_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			if ($this->_socket === false) {
				throw new Exception('Impossible de cr�er le socket IRC !',0);
			}

			if (self::getConfig('ip') != false && self::getConfig('ip') != "") {
				socket_bind($this->_socket, self::getConfig('ip'));
			}
			
			$x = self::getConfig('server');
			$socketUp = socket_connect($this->_socket, self::getConfig('server'), self::getConfig('port'));
			if ($socketUp===false) {
				throw new Exception('Impossible de se connecter au server IRC ! ('.socket_strerror ( socket_last_error() ).' (errno:'.socket_last_error().'))' ,0);
			}

			$this->connected = true;

			if (self::getConfig('password') !== false) {
				$this->put('PASS '.self::getConfig('password'));
			}
			// TODO : be sure for the validity of the connection password (what chain server return if fail ?)

			$this->put('USER '.self::getConfig('nick').' '.self::getConfig('nick').'@'.self::getConfig('ip').' '.self::getConfig('server').' :'.self::getConfig('nick'));
			$this->put('NICK  '.self::getConfig('nick'));

			/*$this->tick->setTick('all5sec',5);
			$this->tick->addJob('all5sec','hello chan','privmsg',array(self::$channel,"I'm a tick. I show this msg all 5sec."),0,$this);
			*/
			
			// Into the while !
			while (1) {
				
				// current data
				$this->incomingData = $this->getIncomingData();
				
				if (!is_array($this->incomingData)) {
					$this->incomingData = array($this->incomingData);	
				}
				
				foreach ($this->incomingData as $data) {
					if (!empty($data)) {
						$event = $this->event()->setIncoming($data);
						
						$action = $event->getAction();
						
						switch ($action) {
							case Event::ACT_DISCONNECT:
								$this->disconnect('ERROR: Closing Link');
								sleep(3);
								throw new Exception('Closing Link.',1);
								break;
							case Event::ACT_PING:
								$pong = split(':',$data);
								$this->put('PONG '.$pong[1]);
					    		echo "PING :{$pong[1]}\nPONG {$pong[1]}\n\n";
					    		break;
							case Event::ACT_KICK:
								sleep(1);
								$this->joinChannel(self::getConfig('channel'));
								break;
						}
						
						$this->dataInformation = $dataInformation = $event->getData();

						switch ($dataInformation['type']) {
				
							case Plugins_Command_Abstract::EVENT_PRIVMSG:
				
								// ctcp
								if ($event->isCtcp()) {
									$ctcp = new IRCMain_Ctcp($action,$this);
									$responce = $ctcp->getResponce();
									
									self::notice($event->getDataSendBy(),$this->ctcp($responce));
								} else {
									if (self::_authentification($event)) {
										continue;
									}
								}
								break;
							case 'NOTICE':
								break;
						}
						//$this->plugins()->set_event($event);
					}
				}
			}
		} catch (myRuntimeException $e) {
			$x = $e->getMessage();
			echo $x;
			$x.= backtrace($e->getTrace());
			file_put_contents('errorlog',$x);
			//if ($e->_level < 2 || ($e->_level >= 256 && $e->_level < 1024)) {
				echo 'Error level : '.$e->_level."\n\n";
				throw new Exception('Error occured',0);
			/*} else {
				echo 'Error level : '.$e->_level."\n\n\n\n";
				throw new Exception("Error occured. Please see errorlog for details.",1);
			}*/
		} catch (Exception $e) {
			throw $e;
		}
	}
}

?>