<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function debug($set=null) {
	static $debug;

	if (!is_null($set)) {
		$debug = $set;
	} else {
		return $debug ? true : false;
	}
}

/**
 * print_r avec un jolis formatage
 *
 * @param mix $v
 */
function xdump($v) {
	echo "\n"; print_r($v); echo "\n";
}

/**
 * var_dump avec un jolis formatage
 *
 */
function xvdump() {
	$args = func_get_args();
	echo "\n"; call_user_func_array('var_dump',$args); echo "\n";
}

function backtrace($backtrace) {
	$err_backtrace = " Backtrace :\n=============\n\r";
	foreach($backtrace as $t) {
		$err_backtrace.= 'File : '.((isset($t['file'])) ? $t['file']:'[PHP KERNEL]').((isset($t['line'])) ? ':'.$t['line'] : '') . "\n".
			'Function : '.$t['function']."\n";
		if (isset($t['class'])) {
			$err_backtrace.= 'Class : '.$t['class']."\n";
		}
		if (isset($t['type'])) {
			$err_backtrace.= 'Type : '.$t['type']."\n";
		}
		/*if (isset($t['args']) && !empty($t['args']) && $t['function'] != 'myErrorHandler') {
			$err_backtrace.= '--== Args ==--'."\n";
			ob_start();
			var_dump($t['args']);
			$err_backtrace.= "\t".str_replace("\n","\n\t",ob_get_contents());
			ob_end_clean();
		}*/
		$err_backtrace.= "\n\n- - - - - - - - - - - - -\n\r";
	}
	return $err_backtrace;
}

/**
 * RuntimeException encapsule une erreur (E_WARNING, E_NOTICE, ...) PHP dans un objet h�ritant de Exception
 */
class myRuntimeException extends Exception {

	/**
	 * Context d'�x�cution fournit par PHP
	 * @var array
	 * @access protected
	 */
	protected $_context = array();
	public $_level;

	/**
	 * Construit un objet RuntimeException
	 * @param int $level Le niveau de l'exception
	 * @param str $string La description de l'exception
	 * @param str $file Le nom du fichier o� l'erreur s'est produite
	 * @param int $line La ligne  du fichier o� l'erreur s'est produite
	 * @param array $context Le context d'�x�cution fournit par le gestionnaire d'erreur
	 * @access system
	 * @return void
	 */
	function __construct($level, $string, $file, $line, $context){
		parent::__construct($string.' in file '.$file.' at line '.$line);
		// on modifie la ligne et le fichier pour ne pas avoir la ligne et le fichier d'o� l'exception est lev�e
		$this->file = $file;
		$this->line = $line;
		$this->_level = $level;
		$this->_context = $context;
	}

}

/**
 * Remplacant le gestionnaire d'erreur natif PHP. IL permet de lever une exception capturable dans un bloc catch
 * pour les fonction/methode ne levant pas d'exception.
 * @param int $level Niveau de l'erreur PHP
 * @param str $string Description de l'erreur
 * @param str $file Chemin d'acc�s au fichier dans lequel l'erreur s'est produite
 * @param int $line Ligne de $file o� l'erreur s'est produite
 * @param array $context Un contexte d'�x�cution fournit par PHP
 * @access system
 * @return void
 *
 */
function myErrorHandler($level, $string, $file, $line, $context){

	throw new myRuntimeException($level, $string, $file, $line, $context);

}

?>