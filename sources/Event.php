<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Event {
	const ACT_DISCONNECT = 'disconnect';
	const ACT_PING       = 'ping';
	const ACT_KICK       = 'kick';
	const ACT_001        = '001'; // 'ok';
	
	const ACT_311        = 'RPL_WHOISUSER';
	const ACT_312        = 'RPL_WHOISSERVER';
	const ACT_313        = 'RPL_WHOISOPERATOR';
	const ACT_314        = 'RPL_WHOWASUSER';
	const ACT_317        = 'RPL_WHOISIDLE';
	const ACT_318        = 'RPL_ENDOFWHOIS';
	const ACT_319        = 'RPL_WHOISCHANNELS';
	const ACT_322        = 'RPL_LIST';
	const ACT_375        = 'RPL_MOTDSTART';
	const ACT_372        = 'RPL_MOTD';
	const ACT_376        = 'RPL_ENDOFMOTD';
	
	const ACT_401        = 'ERR_NOSUCHNICK';
	const ACT_404        = 'ERR_NOSUCHSERVER';
	const ACT_403        = 'ERR_NOSUCHCHANNEL';
	const ACT_432        = 'ERR_ERRONEUSNICKNAME'; // 'IllegalsCharactersInNickname';
	const ACT_433        = 'ERR_NICKNAMEINUSE'; // 'NickAlreadyInUse';
	const ACT_PRIVMSG    = 'privmsg';
	const ACT_NOTICE     = 'notice';
	
	
	/**
	 * IRCMain object
	 *
	 * @var IRCMain
	 */
	private $_ircmain;
	
	protected $incoming;
	/**
	 * Le message pars� et d�pess� en cas de PRIVMSG ou NOTICE
	 *
	 * @var array
	 */
	protected $data;
	
	/**
	 * Quelques informations suppl�mentaire si ce n'est pas un message
	 *
	 * @var array
	 */
	protected $extraData;
	
	public $onMOTD = false;
	
	function __construct (IRCMain $ircmain) {
		$this->_ircmain = $ircmain;
	}
	
	/**
	 * Initialise les donn�es re�ue.
	 *
	 * @param string $message Raw data
	 */
	public function setIncoming($message) {
		$this->incoming = $message;
		return $this;
	}
	
	/**
	 * Return parsed raw incoming message
	 *
	 * @return array
	 */
	public function getData() {
		return $this->data; 
	}
	
	/**
	 * Return the appropriate action to do.
	 *
	 * @return string
	 */
	function getAction() {
		
		echo debug() ? "Raw:: -> ".$this->incoming."\n" : '';
		
		if (preg_match("`^ERROR :(Closing Link: )?(.*)$`i", $this->incoming)) {
			return self::ACT_DISCONNECT;
		} elseif (ereg("^PING ", $this->incoming)) {
			return self::ACT_PING;
		} elseif (preg_match('`^:(.*?)!.*?@.*? KICK '.$this->_ircmain->getConfig('channel').' '.preg_quote($this->_ircmain->getConfig('nick'), '`').' :`', $this->incoming, $T)) {
			return self::ACT_KICK;
		} elseif (preg_match('`^:[^ ]+ ([0-9]{3}) (.*?)`', $this->incoming,$T)) {
			switch ($T[1]) {
				case 001:
					$this->_ircmain->joinChannel($this->_ircmain->getConfig('channel'));
					return self::ACT_001;
					break;
				case 311:
					return self::ACT_311;
				case 312:
					return self::ACT_312;
				case 313:
					return self::ACT_313;
					break;
				case 372: case 375: // motd
					$this->onMOTD = true;
					echo $this->incoming;
					break;
				case 376:
					$this->onMOTD = false;
					break;
				case 432:
					$this->_ircmain->newNick('NoNameBot'.rand(0,9).rand(0,9));
					return self::ACT_432;
					break;
				case 433:
					echo "Nick already in use\n\n";
					$this->_ircmain->newNick(false);
					return self::ACT_433;
					break;
				default:
					echo "I got a responce from the server, but the code was not grabbed :\n";
					echo "Event::DEBUG :> code : $T[1]\n";
					echo "Event::DEBUG :> raw  : ".$this->incoming."\n";
					break;
			}
		} else {
			echo "\n";
			echo "Event::DEBUG(Line not grabbed) :>".$this->incoming;
			echo "\n";
		}

		// :<Owner:T1> PRIVMSG <recever:T2> :<msg:T3>
		if (preg_match('`^:(.*?)!.*?@.*? PRIVMSG ('.$this->_ircmain->getConfig('nick').'|'.$this->_ircmain->getConfig('channel').") :(.*)`",$this->incoming,$T)) {
			
			$this->data = array (
				'type' => Plugins_Command_Abstract::EVENT_PRIVMSG,
				'from' => $T[1], // owner
				'to' => $T[2], // message for bot or channel
				'message' => $T[3]
			);
			return self::ACT_PRIVMSG;
		} elseif (preg_match('`^:(.*?)!.*?@.*? NOTICE '.$this->_ircmain->getConfig('nick')." :(.*)`",$this->incoming,$T)) {
			
			$this->data = array (
				'type' => Plugins_Command_Abstract::EVENT_NOTICE,
				'from' => $T[1],
				'to' => $this->_ircmain->getConfig('nick'),
				'message' => $T[2]
			);
			return self::ACT_NOTICE;
		}
	}
	
	/**
	 * Return the message
	 *
	 * @return string
	 */
	public function getDataMessage() {
		return $this->data['message'];
	}
	
	/**
	 * Return the nick sender
	 *
	 * @return string
	 */
	public function getDataSendBy() {
		return $this->data['from'];
	}
	
	/**
	 * Return the destination
	 *
	 * @return string
	 */
	public function getDataFor() {
		return $this->data['to'];
	}
	
	/**
	 * return the type of event (PRIVMSG or NOTICE)
	 *
	 * @return string
	 */
	public function getDataType() {
		return $this->data['type'];
	}

	/**
	 * Return true if the bot will respond to the channel
	 *
	 * @return boolean
	 */
	public function isForChannel() {
		if ($this->data['to'] == $this->_ircmain->getConfig('nick')) {
			return false;
		}
		return true;
	}
	
	/**
	 * Return true if the message is a CTCP resquest
	 *
	 * @return boolean
	 */
	public function isCtcp() {
		if ($this->data['message'][0] == chr(001)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function onPrivmsg() {}
	public function onCtcp() {}
	public function onMotd() {}
	public function onUnknow() {} 
}

?>
