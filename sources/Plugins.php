<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   IrBot
 * @package    IrBot_Plugins
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

class plugins extends RecursiveDirectoryIterator {
	const EVENT_PRIVMSG = "privmsg";
	const EVENT_NOTICE  = "notice";
	
	private $ircmain;
	protected $text_format;
	
	protected $_events = array(
		self::EVENT_NOTICE  => true,
		self::EVENT_PRIVMSG => true
	);
	
	protected $commands = array();
	
	public function __construct(IRCMain $ircmain) {
		$this->ircmain = $ircmain;
		parent::__construct(BASE_DIR.'plugins/');
		
		while(self::valid()) {
			if (self::isDot()) {
				self::next();
			}
			if (self::isFile()) {
				if (self::isReadable()) {
					$pluginFileName = self::getFilename();
				}
			}
			self::next();
		}
		
	}
	
	public function addPlugin(Plugins_Abstract $plugin) {
	}
	
	public function add_command($plugin_name,Plugins_Command_Abstract $method_to_add,$do_on) {
		if (isset($this->commands[$plugin][$method_to_add])) {
			echo debug() ? 'plugins::add_command() -> ' . "$plugin -> $method_to_add\t\t\ALREADY LOADED\n" : '';
			return true;
		}

		if (!in_array($do_on,$this->accepted_event)) {
			echo debug() ? 'plugins::add_command() -> ' . "$plugin -> $method_to_add\t\t\tFAILED\n" : '';
			return false;
		}

		$this->commands[$plugin][$method_to_add] = array(
			'requier_args'  => $method_to_add->requieredArgs,
			'accepted_args' => $method_to_add->acceptedArgs,
			'help'          => $method_to_add->helpMessage(),
			'type'          => $method_to_add->commandVisibility,
			'do_on'         => $do_on,
		);

		echo debug() ? 'plugins::add_command() -> ' . "$plugin -> $method_to_add\t\t\LOADED\n" : '';
		return true;
	}
}


/*
class plugins {

	public $plugin_list = array();
	private $ircmain;

	public $commands;
	public $command_list;
	public $method_on = array();
	public $accepted_event = array();

	function __construct() {
		$this->ircmain = bot::GetInstance();
		$this->text_format = text_format::GetInstance();
		$this->accepted_event = array('PRIVMSG','NOTICE');
	}

	public function Init() {
		self::build_command_list();
		self::build_buffer();
	}

	public function load_plugin($plugin_name) {
		if ( !array_key_exists ( $plugin_name, $this->plugin_list ) ) {
			if ( is_readable('./plugins/'.$plugin_name.'-plugin.inc.php') ) {
				if (debug()) {
					echo "plugins::load_plugin()[$plugin_name]\n";
				}
				require_once './plugins/'.$plugin_name.'-plugin.inc.php';
				$this->plugin_list[$plugin_name] = new $plugin_name($this);
				self::plugin_register_cmd($plugin_name, $this->plugin_list[$plugin_name]->commands_list());
			} else {
				throw new Exception("Class '$plugin_name' not found",0);
			}
		}
	}

	protected function plugin_register_cmd($plugin,$commands) {
		foreach ($commands as $method => $info) {
			if (!method_exists($this->plugin_list[$plugin],$method) || !is_callable(array($this->plugin_list[$plugin],$method))) {
				trigger_error("method '$plugin:$method' in the commands list is not callable.\n",E_USER_WARNING);
			} else {
				self::add_command($plugin,$method,$info['accepted_args'],$info['help'],$info['type'],$info['requier_args'],$info['do_on']);
			}
		}
	}

	public function unload_plugin($plugin_name) {
		if ($plugin_name == '_all') {
			echo "Unload all plugins ...";
			foreach($this->plugin_list as $pname => $tmp) {
				unset ($this->plugin_list[$pname]);
			}
			echo "\t\t\tdone.\n";
		}
		if ( array_key_exists ( $plugin_name, $this->plugin_list ) ) {
			unset( $this->plugin_list[$plugin_name] );
		}
	}

	public function do_command ($msg_info,$plugin,$method) {
		$all_args = array_slice(func_get_args(), 3);

		if (isset($this->commands[$plugin])) {
			foreach ($this->commands[$plugin] as $method_name => $methods) {
				if ($method_name == $method) {
						$accepted_args = $methods['accepted_args'];

					if ( $accepted_args >= 1 )
						$the_args = array_slice($all_args, 0, $accepted_args);
					elseif ( $accepted_args == 0 )
						$the_args = NULL;
					else
						$the_args = $all_args;

					$call_args = count($all_args);

					if (debug()) {
						echo "plugins::do_command()[$plugin::$method_name] -> type : {$methods['type']}\n";
						echo "plugins::do_command()[$plugin::$method_name] -> accepted_args : $accepted_args, requier_args : {$methods['requier_args']}, args : ".$call_args."\n\n\n";
					}

					$dest_error_prototype = "This command must be called in %s mode. Try /msg %s !$plugin $method_name";
					$fail = false;

					switch ($methods['type']) {
						case 'private':
							if ($msg_info['to'] != bot::$myBotName) {
								$msg = sprintf($dest_error_prototype,'private',bot::$myBotName);
								$fail = true;
							}
							break;
						case 'public':
							if ($msg_info['to'] != bot::$channel) {
								$msg = sprintf($dest_error_prototype,'public', bot::$channel);
								$fail = true;
							}
							break;
						case 'mixed':
							// no reason to match this...
							break;
					}

					if ($fail == true) {
						if ($methods['requier_args'] > 1) {
							$i=0;
							while ($i<$methods['requier_args']) {
								$msg.= " arg,";
								$i++;
							}
							$msg = substr($msg,0,-1);
							$msg.= "\n".text_format::bold('You must enter '.$methods['requier_args'].' arguments.');
						}
						$this->ircmain->mprivmsg($msg_info['from'],text_format::paragraphe($msg));
						return false;
					}

					if ($call_args < $methods['requier_args']) {
						$msg = "There is an error with the number of arguments. You need to enter on minimum {$methods['requier_args']} arguments for this command.";
						$this->ircmain->privmsg($msg_info['from'],$msg);
						//call_user_func_array(array($this->plugin_list[$plugin],'error'),array($msg,$method_name));
						return false;
					} elseif ($call_args > $accepted_args) {
						$msg = "There is an error with the number of arguments. You need to enter on maximum {$methods['requier_args']} arguments for this command.";
						$this->ircmain->privmsg($msg_info['from'],$msg);
						//call_user_func_array(array($this->plugin_list[$plugin],'error'),array($msg,$method_name));
						//$this->ircmain->privmsg($msg_info['from'],"Error with num args");
						return false;
					}

				//	echo "Args :\n";
				//	print_r($the_args);
				//	echo "\n";

					if ($plugin == 'core') {
						call_user_func_array(array($this->ircmain,$method_name),$the_args);
					} else {
						$this->plugin_list[$plugin]->current_message($msg_info);
						call_user_func_array(array($this->plugin_list[$plugin],$method_name),$the_args);
					}
					return true;
				}
			}
		}
	}

	public function add_command($plugin,$method_to_add,$accepted_args=0,$help='',$type='public',$requier_args=0,$do_on) {
		if (isset($this->commands[$plugin][$method_to_add])) {
			echo debug() ? 'plugins_controller::add_command() -> ' . "$plugin -> $method_to_add\t\t\ALREADY LOADED\n" : '';
			return true;
		}

		if (!in_array($do_on,$this->accepted_event)) {
			echo debug() ? 'plugins_controller::add_command() -> ' . "$plugin -> $method_to_add\t\t\tFAILED\n" : '';
			return false;
		}

		$this->commands[$plugin][$method_to_add] = array(
			'requier_args'  => $requier_args,
			'accepted_args' => $accepted_args,
			'help'          => $help,
			'type'          => $type,
			'do_on'         => $do_on,
		);

		echo debug() ? 'plugins_controller::add_command() -> ' . "$plugin -> $method_to_add\t\t\LOADED\n" : '';
		return true;
	}

	public function remove_command($plugin,$method_to_remove) {
		if ($method_to_remove == '_all_method') {
			unset($this->commands[$plugin]);
		} elseif (isset($this->commands[$plugin][$method_to_remove])) {
			unset($this->commands[$plugin][$method_to_remove]);

			$this->commands[$plugin] = $new_method_list;
		}
		self::build_command_list();
		return true;
	}

	public function list_command($plugin) {
		if (isset($this->commands[$plugin])) {
			foreach ($this->commands[$plugin] as $method_name => $method_info) {
				$command_list[] = array_merge(array('method'=>$method_name),$method_info);
			}
			return $command_list;
		}
		return false;
	}

	public function command_exist($command) {
		if (isset($this->command_list[$command])) {
			return true;
		} else {
			return false;
		}
	}

	public function set_event($data) {
		switch ($data['type']) {
			case 'PRIVMSG':
				self::do_on_privmsg($data);
			break;
			case 'NOTICE':
				self::do_on_notice($data);
				break;
		}

		return;

		if ($data['message'][0] == '!') {
			$message = substr($data['message'],1);
			$query = explode(' ',$message);
			$query_count = count($query);

			if (isset($this->plugins->commands[$query[0]])) {
				if ($query_count > 1) {
					if (isset($this->plugins->commands[$query[0]][$query[1]])) {
						call_user_func_array(array($this->plugins,'do_command'),array_merge(array('msg_info'=>$msg_info),$query));
					} else {
						bot::GetInstance()->privmsg($data['from'],"This command do not exist. Try !{$query[0]} for the list of commands avaiable with this plugin.");
					}
				} else {
					// no method : need a list of commands
					$plugin = $message;

					$commands = $this->plugins->list_command($plugin);
					foreach ($commands as $command_info) {
						if ($command_info['type'] == 'public') {
							$get_plugin_method = '!'.$plugin;
						} else {
							$get_plugin_method = '/msg ' . bot::$myBotName . ' !' . $plugin;
						}

						$msg = array_merge(array(bot::GetInstance()->formater->bold('Command :')." $get_plugin_method {$command_info['method']}".(($command_info['accepted_args']>0)?' [some args...]':'')),text_format::paragraphe($command_info['help']));
						bot::GetInstance()->mprivmsg($msg_info['from'],$msg);
					}
				}
			} else {
				//var_dump($this->plugins->commands);
			}
		}
	}

	private function do_on_privmsg() {
		foreach ($this->method_on['privmsg'] as $method_name) {
			//
		}
	}
	private function do_on_notice() {
		foreach ($this->method_on['notice'] as $method_name) {
			//
		}
	}

	private function build_command_list() {
		foreach ($this->commands as $plugin => $method_data) {
			foreach ($method_data as $method_name => $method_info) {
				$this->command_list = array(
					$method_name => array(
						'plugin' => $plugin,
						'method_info' => $method_info,
					)
				);
				switch($method_info['do_on']) {
					case 'PRIVMSG':
						$this->method_on['privmsg'] = $method_name;
						break;
					case 'NOTICE':
						$this->method_on['privmsg'] = $method_name;
						break;
				}
			}
		}
		return true;
	}
}
*/
?>