<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class IRCMain_Ctcp {
	
	const CTCP_CLIENTINFO = 'CLIENTINFO';
	const CTCP_VERSION    = 'VERSION';
	const CTCP_USERINFO   = 'USERINFO';
	const CTCP_TIME       = 'TIME';
	const CTCP_PING       = 'PING';
	
	/**
	 * Event object
	 *
	 * @var Event
	 */
	protected $_event;
	
	/**
	 * IRCMain object for config data
	 *
	 * @var IRCMain
	 */
	protected $_ircmain;
	
	public $ctcpNotice;
	public $ctcpRequest;
	public $ctcpQuery;
	
	public function __construct(Event $event, IRCMain $ircmain) {
		$this->_event = $event;
		$this->_ircmain = $ircmain;
	}
	
	function _stripMessage() {
		$this->ctcpNotice = str_replace(chr(001), "", $this->_event->getDataMessage());
	}
	
	function _setRequest() {
		
		self::_stripMessage();
		
		if (strstr($this->ctcpNotice,' ') === true) {
			list($this->ctcpRequest,$this->ctcpQuery) = explode(" ",$this->ctcpNotice);
			$this->ctcpRequest = trim($this->ctcpRequest);
		} else {
			$this->ctcpRequest = trim($this->ctcpNotice);
		}
		
	}
	
	/**
	 * Return the ctcp responce
	 *
	 * @return string
	 */
	function getResponce() {
		switch ($this->ctcpRequest) {
			case self::CTCP_CLIENTINFO:
				return 'PING VERSION TIME USERINFO CLIENTINFO';
				break;

			case self::CTCP_VERSION:
				return 'IrBot version '.$this->_ircmain->getConfig('version').' - PHP  '.phpversion().' -- on http://irbot.irstat.org';
				break;

			case self::CTCP_USERINFO:
				return 'IrBot';
				break;

			case self::CTCP_TIME:
				return date('Y-m-d H:i:s');
				break;

			case self::CTCP_PING:
				return "PING " . $query;
				break;
			default:
				return "UNKNOWN CTCP REQUEST : '$this->ctcpRequest'";
				break;
		}
	}
}
?>