<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   IrBot
 * @package    IrBot_IRCMain
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * @see Zend_Registry
 */
require_once "sources/Registry.php";

abstract class IRCMain_Adapter {
	
	static public $_instance = false;
	static public $_pluginsInstance = false;
	
	protected $_socket;
	protected $_lastTimeData = 0;

	protected $_config = array(
		'botVersion'    => '1.0',
		'server'        => 'localhost',
		'port'          => 6667,
		'channel'       => '#test',
	    'nick'          => 'irBot',
	    'ip'            => '',
	    'domain'        => '',
	    'password'      => false,
	    'nickserv'      => false,
		'socketTimeout' => 180,
	);
	
	abstract function __construct(array $options);

	protected function _checkTimeout() {
		$now = mktime();
		if ($this->_lastTimeData+self::getConfig('socketTimeout') < mktime()) {
			throw new Exception('Connection lost (Timeout).',1);
		}
		$lag = $now-$this->_lastTimeData;
		if ( $lag > 20 && ($lag % 10) == 0) {
			echo "Lag: ".$lag." seconds\n";
		}
	}

	protected function getIncomingData() {

		$buffer = '';
		socket_set_nonblock($this->_socket);

		while (1) {
			//echo "TOC - ",time(),"\n";
			//$this->tick->doAllTicks();
			$buf = @socket_read($this->_socket, 4096);

			if (empty($buf)) {
				$this->_checkTimeout();
				sleep(1);
				continue;
			}

			$this->_lastTimeData = mktime();

			if (!strpos($buf, "\n")) { //Si ne contient aucun retour, on bufferise
				$buffer = $buffer.$buf;
				$data = ""; //rien � envoyer
			} else {
				//Si contient au moins un retour,
				//on v�rifie que le dernier caract�re en est un
				if (substr($buf, -1, 1)  == "\n") {
					//alors on additionne ces donn�es au buffer
					$data = $buffer.$buf;
					$buffer = ""; //on vide le buffer
				} else  {
					//si le dernier caract�re n'est pas un retour � la
					//ligne, alors on envoit tout jusqu'au dernier retour
					//puis on bufferise le reste
					$buffer = $buffer.substr($buf, strrchr($buf, "\n"));
					$data = substr($buf, 0, strrchr($buf, "\n"));
					$data = $buffer.$data;
					$buffer = ""; //on vide le buffer
				}
			}

			if ($data != '') {
				$data = split("\n", $data);
				return $data;
			}

			continue;
		}
	}
	
	public function launch() {
		//$this->_pluginsInstance = new plugins($this);
		$this->_lastTimeData = mktime();
	}
	
	public function plugins() {
		if ($this->_pluginsInstance instanceof plugins) {
			return $this->_pluginsInstance;
		} else {
			throw new Exception('Plugins is not initialized');
		}
	}
	
	protected function event() {
		return new Event($this);
	}
	
	public function getConfig($name) {
		return array_key_exists($name, $this->_config) ? $this->_config[$name] : false ;
	}
	
	public function setConfig(array $options) {
		foreach ($options as $option => $value) {
			if ($option == 'botVersion') {
				return false;
			}
			
			$this->_config[$option] = $value;
		}
		return true;
	}
	
	public function joinChannel($channel) {
		$this->put('JOIN '.$channel);
		echo "Join channel $channel ...\n";
	}
	
	final protected function nick_change() {
		echo "New nick : ".self::getConfig('nick')."\n";
		if ($this->put('NICK :'.self::getConfig('nick'))) {
			return true;
		}
		return false;
	}

	public function newNick($new=false) {
		switch ($new) {
			case self::getConfig('nick'):
				echo "New nick : [ERR] no changes :". self::getConfig('nick') . ' == ' . $new ."\n";
				break;
			case false:
				self::setConfig(array(
					'nick' => self::getConfig('nick').'_',
				));
				
				self::nick_change();
				break;
			default:
				self::setConfig(array(
					'nick' => $new
				));
				
				self::nick_change();
				break;
		}
	}

	/**
	 * Envoie une notice a un salon / utilisateur
	 *
	 * @param string $to
	 * @param string $message
	 */
	public function notice ($to,$message) {
		self::put('NOTICE '.$to.' :'.$message."\n");
	}

	/**
	 * Envoie un message (PRIVMSG) a un salon / utilisateur
	 *
	 * @param string $to
	 * @param string $message
	 * @todo wrap message to 512 char (max)
	 */
	public function privmsg ($to,$message) {
		$search = array('#name');
		$replace = array(self::$myBotName);
		$message = str_replace($search,$replace,$message);
		self::put('PRIVMSG '.$to.' :'.$message."\n");
	}

	/**
	 * Multi message to send.
	 *
	 * @param string $to The reveiver
	 * @param array $messages The messages
	 * @param int $interval Delay execution in microseconds
	 */
	public function mprivmsg($to,array $messages,$interval=650000) {
		if (is_array($messages)) {
			foreach ($messages as $msg) {
				$this->privmsg($to,$msg);
				usleep($interval);
			}
		}
	}

	/**
	 * Send data to server.
	 * Return false if it fail
	 *
	 * @param string $data Raw data to send
	 * @return boolean
	 */
	public function put($data) {
		if (!is_resource($this->_socket)) {
			throw new Exception('Connection lost...',1);
			return;
		}

		echo debug() ? 'core::put() -> ' . $data . "\n" : '';

		$ok = socket_write($this->_socket, $data . "\n");
		if ($ok) {
			return true;
		} else {
			return false;
		}

		return true;
	}
	
	public function colors($msg,$colorText,$colorBackground=false) {
		$colorTag = chr(3);
		$first = ($background) ? $colorTag . $colorText . ',' . $colorBackground : $colorTag . $colorText;
		return $first.$msg.$colorTag;
	}

	public function bold($msg) {
		return chr(2).$msg.chr(2);
	}

	public function ctcp($msg) {
		return chr(1).$msg.chr(1);
	}

	public function reverse_color($msg) {
		return chr(22).$msg.chr(22);
	}

	public function underline($msg) {
		return chr(31).$msg.chr(31);
	}

	/**
	 * Return an array for multilines paragraphe
	 *
	 * @param string $msg Message
	 * @return array
	 */
	public function paragraphe($msg) {
		return explode("\n",$msg);
	}
	
	/**
	 * Return the nickname of the bot
	 *
	 * @return string
	 */
	public function getBotName() {
		return self::getConfig('nick');
	}
	
	public function restart() {
		if ($this->auth) {
			echo 'Restart...'."\n";
			$this->disconnect('Restarting, please wait ...');
			sleep(5);
			throw new Exception('Restart...',1);
		} else {
			$this->privmsg($this->msg_info['from'],"Vous n'etes pas authentifie.");
		}
	}

	public function quit() {
		if ($this->auth) {
    		$this->disconnect();
    		throw new Exception('Quit from',3);
		} else {
			$this->privmsg($this->msg_info['from'],"Vous n'etes pas authentifie.");
		}
	}

	public function disconnect($msg='EOL ;') {
		//$this->plugins->unload_plugin('_all');
		echo "core::disconnect() -> Closing Link...";
		if (is_resource($this->_socket)) {
			if ($this->put('QUIT :'.$msg)) {
				$this->connected = false;
				socket_close($this->_socket);
			}
			echo "\t\t\tdone.\n";
		} else {
			echo "\t\t\tError.\n";
			echo "core::disconnect() -> Connection already breack down.\n";
		}
		return true;
	}
	
	function __destruct() {
		if (is_resource($this->_socket)) {
			echo "Error occured.";
			if ($this->put('QUIT :Error occured')) {
				socket_close($this->_socket);
			}
		} else {
			echo "\n\nAll process shut down, bye.";
		}
	}
}

?>