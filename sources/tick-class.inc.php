<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  LICENCE
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @category   IrBot
 * @package    IrBot_Tick
 * @copyright  Copyright (c) 2007-2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 * 
 */

/**
 * Cron event emulation
 *
 * @category   IrBot
 * @package    IrBot_Tick
 * @copyright  Copyright (c) 2007-2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */
class tick {
	private $ticks = array();
	/**
	 * IRCMain object
	 *
	 * @var IRCMain
	 */
	private $ircmain;

	static public $instance = false;

	public static function GetInstance(IRCMain $ircmain) {
		if (!self::$instance) {
			self::$instance = new tick($ircmain);
		}
		return self::$instance;
	}

	private function __construct(IRCMain $ircmain) {
		$this->ircmain = $ircmain;
	}

	/**
	 * Adding a job to the tick list $tickname
	 *
	 * @param string $tickname
	 * @param string $jobname
	 * @param string $function Function to call
	 * @param array $params Parameters send to the $function
	 * @param boolean $temporary Make the job temporary (will be deleted on next execution of the tick)
	 * @param object $object Reference to an object if $function is a method
	 * @return boolean
	 */
	public function addJob($tickname,$jobname,$function,$params,$temporary=0,$object=null) {
		if (isset($this->ticks[$tickname])) {
			if (!is_null($object)) {
				$this->ticks[$tickname]['jobs'][] = array(
					'jname' => $jobname,
					'function' => $function,
					'params' => $params,
					'temporary' => $temporary,
					'objectRef' => &$object
				);
			} else {
				$this->ticks[$tickname]['jobs'][] = array(
					'jname' => $jobname,
					'function' => $function,
					'params' => $params,
					'temporary' => $temporary
				);
			}
			return true;
		}
		return false;
	}

	/**
	 * Create a tick if not exist
	 *
	 * @param string $name
	 * @param int $timer Number of seconds before the next tick
	 * @return boolean
	 */
	public function setTick($name,$timer) {
		if (!isset($this->ticks[$name])) {
			$this->ticks[$name] = array(
				'timer' => $timer,
				'reference' => time(),
				'tickOn' => strtotime("+$timer seconds"),
				'jobs' => array()
			);
			return true;
		}
		return false;
	}

	/**
	 * Delete a tick
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function delTick($name) {
		if (isset($this->ticks[$name])) {
			unset($this->ticks[$name]);
			return true;
		}
		return false;
	}

	public function doAllTicks() {
		$curtime = time();

		foreach($this->ticks as $name => $options) {

			if ($options['tickOn']<=$curtime) {
				echo $options['tickOn']," - $curtime\n";

				foreach ($options['jobs'] as $functions) {

					if (!$functions['temporary']) {
						$jlist[] = $functions;
					}

					if (!$this->ircmain->connected) {
						continue;
					}

					switch (isset($functions['objectRef'])) {
						case true:
							if (!is_object($functions['objectRef'])) {
								throw new Exception('The objectRef for tick '.$name.' at job '.$functions['jname'].' is not an object.',0);
							}
							//print_r($functions);break;
							call_user_func_array(array($functions['objectRef'],$functions['function']),$functions['params']);
							break;

						case false:
							call_user_func_array($functions['function'],$functions['params']);
							break;
					}

				}

				$options['jobs'] = $jlist;

				$this->ticks[$name] = array(
					'timer' => $options['timer'],
					'reference' => time(),
					'tickOn' => strtotime("+{$options['timer']} seconds"),
					'jobs' => $options['jobs']
				);

				echo "newTickOn {$this->ticks[$name]['tickOn']}\n";

			}

		}

		return true;

	}
}

?>