<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   IrBot
 * @package    IrBot_Plugins
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

abstract class Plugins_Command_Abstract {
	
	const REQUIERED_ARGS      = 'requieredArgs';
	const ACCEPTED_ARGS       = 'acceptedArgs';
	const COMMAND_VISIBILITY  = 'commandVisibility';
	const TYPE_PUBLIC         = 'public';
	const TYPE_PRIVATE        = 'private';
	const TYPE_BOTH           = 'both';
	const NOT_PUBLIC_MESSAGE  = 'notPublicMessage';
	const NOT_PRIVATE_MESSAGE = 'notPublicMessage';
	const ALLOW_EMPTY         = 'allowEmpty';
	const NOT_EMPTY_MESSAGE   = 'notEmptyMessage';
	const ON_EVENT            = 'onEvent';
	const EVENT_PRIVMSG       = "privmsg";
	const EVENT_NOTICE        = "notice";
	

	
	/**
	 * Parent object
	 *
	 * @var Plugins_Abstract
	 */
	protected $_plugin;
	
	protected $_helpMessage = '';
	protected $type;
	
	protected $_defaults = array(
		self::REQUIERED_ARGS => 0,
		self::ACCEPTED_ARGS  => 0,
		self::COMMAND_VISIBILITY   => self::TYPE_BOTH,
		self::NOT_PUBLIC_MESSAGE  => 'Sorry, but the command is not for public channel.',
		self::NOT_PRIVATE_MESSAGE => 'Sorry, but the command is not for private query.',
	);
	
	protected $_defaultsArgs = array(
		self::ALLOW_EMPTY    => true,
		
	);
	
	protected $_events = array(
		self::EVENT_NOTICE  => true,
		self::EVENT_PRIVMSG => true
	);
	
	/**
	 * Event object
	 *
	 * @var Event
	 */
	protected $_event;
	
	public function __construct(Plugins_Abstract $plugin) {
		$this->_plugin = $plugin;
	}
	
	public function setOptions(array $options) {
		foreach ($options as $option => $value) {
			switch ($option) {
				case self::ACCEPTED_ARGS:
				case self::REQUIERED_ARGS:
					$this->_default[$option] = $value;
					break;
				default:
					throw new Exception ("Unkown option '$option'");
			}
		}
	}
	
	public function setCurrentEvent (Event $event) {
		$this->_event = $event;
	}
	
	public function helpMessage() {
		return $this->_helpMessage;
	}
	
	public function __get($option) {
		return $this->_default[$option];
	}
	abstract public function __toString();
}
