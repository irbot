<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   IrBot
 * @package    IrBot_Plugins
 * @copyright  Copyright (c) 2008 Belli�re Ludovic
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

abstract class Plugins_Abstract extends RecursiveDirectoryIterator {

	private $_message, $_commands;

	protected $_commandsPath = 'plugins/%pluginName%/';

	final public function __construct($pluginName, $flags=0) {
		self::setCommandPath($pluginName);
		parent::__construct($path,$flags);
		self::buildCommandList();
		self::init();
	}
	
	public function init() {
	}
	
	public function commandList() {
		return $this->_commands;
	}
		
	/**
	 * fetch directory $path and get the filename as command.
	 *
	 */
	protected function buildCommandList() {
		while(self::valid()) {
			if (self::isDot()) {
				self::next();
			}
			if (self::isFile()) {
				if (self::isReadable()) {
					require_once $this->_commandsPath . self::getFilename();
					
					if (!class_exists(basename(self::getFilename(),'.php'))) {
						require_once 'Plugins/Exception.php';
						throw new Plugins_Exception('File '.self::getFilename().' exists, but no object found.');
					}
					
					$this->_commands[] = self::getFilename();
				}
			}
			self::next();
		}
	}
	
	public function setCurrentMessage($message){
		$this->_message = $message;
	}

	protected function setCommandPath($pluginName) {
		$this->_commandsPath = str_replace('%pluginName%',$pluginName);
	}

}
