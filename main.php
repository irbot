#!/usr/bin/php
<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007-2008  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

define('BASE_DIR',dirname(__FILE__).'/');

require_once('./config.php');
require_once('./sources/functions.inc.php');

debug(true);
//set_error_handler('myErrorHandler');

require_once('sources/Registry.php');
Zend_Registry::registerAutoload();

require_once('./sources/IRCMain.php');
//require_once('./sources/Plugins.php');
require_once('./sources/tick-class.inc.php');

echo <<<EOF

	IrBot  Copyright (C) 2007-2008  Belli�re Ludovic
	This program comes with ABSOLUTELY NO WARRANTY;
	This is free software, and you are welcome to redistribute it
	under certain conditions; for details see <http://www.gnu.org/licenses/>.


EOF;

// searching for command line option
if (isset($argv[1])) {
	$i=1;
	while (isset($argv[$i])) {
		$val = false;
		$option = $argv[$i];
		if (substr_count($option,'=') === 1) {
			list($option,$val) = explode('=',$option);
		}
		
		// short option used
		if (!$val && ($option != '-h' && $option != '--help')) {
			$i++;
			$val = $argv[$i];
		}

		switch ($option) {
			case '-h':case '--help':
				echo "
./main.php [options]

Options :
  -s	Equivalent to --server=name
  -p	Equivalent to --port=number
  -c	Equivalent to --channel=#name
  -n	Equivalent to --nick=name
  -i	Equivalent to --ip=number
  -d	Equivalent to --domain=HOST
  -a	Equivalent to --password=pwd
  --server=name
	IRC server to connect (default: ".IRC_SERVER.")
  --port=number
  	Port to use (default: ".IRC_PORT.")
  --channel=#value
  	Channel to join (default: ".IRC_CHANNEL.")
  --nick=nickname
  	Nick of the bot (default: ".IRC_PSEUDO.")
  --ip=xxx.xxx.xxx.xxx
  	Local ip (default: ".IRC_IP.")
  --domain=HOST
  	Local domain (default: ".IRC_DOMAIN.")
  --password=pwd
  	Server password (default: ".IRC_PASSWORD.")
";
				$stop = true;
				break;
			case '-s':case '--server':
				$setServer = $val;
				break;
			case '-p':case '--port':
				$setPort = (int) $val;
				break;
			case '-c':case '--channel':
				$setChannel = $val;
				break;
			case '-n':case '--nick':
				// TODO check for alphanumeric nick name. No utf8 allowed.
				$setNick = $val;
				break;
			case '-i':case '--ip':
				if (ereg("[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}",$val)) {
					$setIp = $val;
				} else {
					echo "The ip adress entered is wrong.\nUsing default ip instead : ".IRC_IP."\n";
				}
				break;
			case '-d':case '--domain':
				$setDomain = $val;
				break;
			case '-a':case '--password':
				$setPassword = $val;
				break;
			default:
				break;
		}
		$i++;
	}
	if (isset($stop)) {
		die();
	}
}

$options = array(
	'server'   => (isset($setServer)) ? $setServer : IRC_SERVER,
	'port'     => (isset($setPort)) ? $setPort : IRC_PORT,
	'channel'  => (isset($setChannel)) ? $setChannel : IRC_CHANNEL,
	'nick'     => (isset($setNick)) ? $setNick : IRC_PSEUDO,
	'ip'       => (isset($setIp)) ? $setIp : IRC_IP,
    'domain'   => (isset($setDomain)) ? $setDomain : IRC_DOMAIN,
    'password' => (isset($setDomain)) ? $setDomain : IRC_PASSWORD,
);

$ircMain = new IRCMain($options);

while (1) {

	// On charge les plugins que l'on souhaite
//	$ircMain->plugins()->load_plugin('sample');
//	$ircMain->plugins()->load_plugin('jet');

	// On lance le bot
	try {
		$ircMain -> launch();
	} catch (Exception $e) {
		switch($e->getCode()) {
			case 0:
				echo $e->getMessage()."\n";
				echo "Process terminating...\n";
				die("EOL from client\n");
				break 2;
			case 1: // SIGHUP restart
				echo $e->getMessage()."\n";
				echo "Process restarting ...\n\n";
				continue 2;
			case 3: // SIGQUIT
				die("EOL from user...\n");
				break 2;
		}
	}
}

?>