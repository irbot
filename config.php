<?php
/**
 *  This file is part of IrBot, irc robot.
 *  Copyright (C) 2007  Belli�re Ludovic
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: config.php 24 2007-12-30 12:43:56Z xrogaan $
 */

error_reporting(E_ALL);

date_default_timezone_set('Europe/Brussels');


define('IRC_SERVER','irc.oh-my-songs.com');
define('IRC_PORT',6667);
define('IRC_CHANNEL','#oms-network');
define('IRC_PSEUDO','irbot');
define('IRC_IP','');
define('IRC_DOMAIN','');
define('IRC_PASSWORD',false);

?>